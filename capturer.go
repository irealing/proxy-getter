package main

import (
	"fmt"
	"github.com/qiniu/log"
)

func main() {
	var cap Capture = &CNProxyAdapter{}
	err := cap.Init()
	if err != nil {
		log.Fatal("初始化失败 ：", err)
	}
	for cap.HasNext() {
		s := cap.Next()
		fmt.Printf("%s:%s\n", s.Host, s.Port)
	}
}
